# include <stdio.h>
# include <conio.h>
# include <math.h>
/* finding the roots of a quadratic equation*/
double main()
{
	float a,b,c,d,root1,root2;
	
	printf("Enter the coefficient of x^2, a:\n");
	scanf("%f",&a);
	while(a == 0){
		printf("Enter another value but not zero (0)\n");
		scanf("%3f",&a);
	}
	printf("Enter the coefficient of x, b:\n");
	scanf("%f",&b);
	printf("Enter the value of the constant, c:\n");
	scanf("%f",&c);
	d = (b*b)-(4*a*c);
	if (d < 0){
		printf("The roots are Complex\n");
	}
	if (d > 0){
		printf("The roots are real and distinct\n");
		root1 = (-b + (sqrt(d)))/(2*a);
		root2 = (-b - (sqrt(d)))/(2*a);
		printf("Either x = %.2f Or x = %.2f\n", root1,root2);
	}
	else if (d == 0){
		printf("The roots are real and equal\n");
		root1 = (-b + (sqrt(d)))/(2*a);
		root2 = (-b - (sqrt(d)))/(2*a);
		printf("Either x = %.2f or x =  %.2f\n", root1,root2);
	}
	return 0;
}
